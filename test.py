import sys

from out.py import example

# used for before/after columns
before = example.cvar.my_variable
example.cvar.my_variable = 5.1
after = example.cvar.my_variable

# used for sum_x() columns
arr = b'\x01\x02\x03\xFF'

# used for append column
s1 = 'abc'

# used for class public variable
app = example.App()
before2 = app.m_variable
app.m_variable = 5.2
after2 = app.m_variable


# --------------------
def print_value(i):
    if i == '0':
        print('{0: >11}'.format('python'), end=' ')
    elif i == '1':
        print('{0: >11}'.format(example.fact(6)), end=' ')
    elif i == '2':
        print('{0: >11}'.format(example.my_mod(11, 8)), end=' ')
    elif i == '3':
        print('{0: >11}'.format(before), end=' ')
    elif i == '4':
        print('{0: >11}'.format(after), end=' ')
    elif i == '5':
        print('{0: >11}'.format(example.sum_int(arr)), end=' ')
    elif i == '6':
        print('{0: >11}'.format(example.sum_byte(arr)), end=' ')
    elif i == '7':
        print('{0: >11}'.format(example.append(s1)), end=' ')
    elif i == '8':
        print('{0: >11}'.format(before2), end=' ')
    elif i == '9':
        print('{0: >11}'.format(after2), end=' ')
    elif i == '10':
        print('{0: >11}'.format(app.append('xy')), end=' ')
    else:
        print(f'unknown cli arg: {i}')


# --------------------
# --- main
if len(sys.argv) < 2 or sys.argv[1] == 'all':
    for i in range(10):
        print_value(str(i))
        print('')
elif sys.argv[1] == 'to_hex':
    arr = b'\x01\x02\x03\xFF\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10'
    print(f"python     : {example.to_hex(arr)}", end='')
else:
    print_value(sys.argv[1])
