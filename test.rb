require_relative './out/rb/example.so'

# used for before/after columns
$before = Example.my_variable
Example.my_variable = 5.1
$after = Example.my_variable

# used for sum_x() columns
$arr = "\x01\x02\x03\xFF".bytes.to_a

# used for append column
$s1 = 'abc'

# used for class public variable
$app = Example::App.new()
$before2 = $app.m_variable
$app.m_variable = 5.2
$after2 = $app.m_variable

# --------------------
def print_value(i)
   if i == 0
      print(sprintf('%11s', 'ruby'))
   elsif i == 1
      print(sprintf('%11d', Example.fact(6)))
   elsif i == 2
      print(sprintf('%11d', Example.my_mod(11, 8)))
   elsif i == 3
      print(sprintf('%11.1f', $before))
   elsif i == 4
      print(sprintf('%11.1f', $after))
   elsif i == 5
      print(sprintf('%11d', Example.sum_int($arr)))
   elsif i == 6
      print(sprintf('%11s', Example.sum_byte($arr)))
   elsif i == 7
      print(sprintf('%11s', Example.append($s1)))
   elsif i == 8
      print(sprintf('%11.1f', $before2))
   elsif i == 9
      print(sprintf('%11.1f', $after2))
   elsif i == 10
      print(sprintf('%11s', $app.append('xy')))
   else
      puts("unknown cli arg #{i}")
   end
end

# --------------------
# --- main
if ARGV.length == 0 or ARGV[0] == 'all'
    (0..10).each do |i|
        print_value(i)
        puts
    end
elsif ARGV[0] == 'to_hex'
     arr = "\x01\x02\x03\xFF\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10".bytes
     puts("ruby       : #{Example.to_hex(arr)}")
else
    print_value(ARGV[0].to_i)
end

