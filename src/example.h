#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <cstdint>
#include <string>
#include <vector>

extern double my_variable;
extern int fact(int);
extern int my_mod(int n, int m);
extern int sum_int(const std::vector<int>& buf);
extern int sum_byte(const std::vector<uint8_t>& buf);
extern std::string append(const std::string& s1);
extern std::string to_hex(const std::vector<uint8_t>& buf);

#endif // EXAMPLE_H
