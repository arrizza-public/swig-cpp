.PHONY : all clean help  ruby_example_wrap ruby_example_wrap-init ruby_example_wrap-gen ruby_example ruby_example-init ruby_example-build ruby_example-shared python_example_wrap python_example_wrap-init python_example_wrap-gen python_example python_example-init python_example-build python_example-shared node_example node_example-init node_example-build node_example-shared
#-- build all
all:  ruby_example_wrap ruby_example_wrap-init ruby_example_wrap-gen ruby_example ruby_example-init ruby_example-build ruby_example-shared python_example_wrap python_example_wrap-init python_example_wrap-gen python_example python_example-init python_example-build python_example-shared node_example node_example-init node_example-build node_example-shared
#-- build ruby_example_wrap
ruby_example_wrap: ruby_example_wrap-init ruby_example_wrap-gen
#-- build ruby_example
ruby_example: ruby_example-init ruby_example-build ruby_example-shared
#-- build python_example_wrap
python_example_wrap: python_example_wrap-init python_example_wrap-gen
#-- build python_example
python_example: python_example-init python_example-build python_example-shared
#-- build node_example
node_example: node_example-init node_example-build node_example-shared

# ==== ruby_example_wrap

#-- ruby_example_wrap: initialize for debug build
ruby_example_wrap-init:
	@mkdir -p debug
	@mkdir -p debug/ruby_example_wrap-dir/src

debug/ruby_example_wrap-dir/src/example.i.cpp: src/example.i
	swig -ruby -c++  -o debug/ruby_example_wrap-dir/src/example.i.cpp src/example.i 

#-- ruby_example_wrap: genned source files
ruby_example_wrap-gen: debug/ruby_example_wrap-dir/src/example.i.cpp 

#-- ruby_example_wrap: clean files in this target
ruby_example_wrap-clean:
	rm -f debug/ruby_example_wrap-dir/src/example.i.cpp/*.cpp

# ==== ruby_example

#-- ruby_example: initialize for debug build
ruby_example-init:
	@mkdir -p debug
	@mkdir -p debug/ruby_example-dir/debug/ruby_example_wrap-dir/src
	@mkdir -p debug/ruby_example-dir/src

-include debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.d
debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.o: debug/ruby_example_wrap-dir/src/example.i.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/ruby-3.2.0" "-I/usr/include/x86_64-linux-gnu/ruby-3.2.0"  -std=c++20 -D_GNU_SOURCE  debug/ruby_example_wrap-dir/src/example.i.cpp -o debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.o
-include debug/ruby_example-dir/src/app.cpp.d
debug/ruby_example-dir/src/app.cpp.o: src/app.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/ruby-3.2.0" "-I/usr/include/x86_64-linux-gnu/ruby-3.2.0"  -std=c++20 -D_GNU_SOURCE  src/app.cpp -o debug/ruby_example-dir/src/app.cpp.o
-include debug/ruby_example-dir/src/example.cpp.d
debug/ruby_example-dir/src/example.cpp.o: src/example.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/ruby-3.2.0" "-I/usr/include/x86_64-linux-gnu/ruby-3.2.0"  -std=c++20 -D_GNU_SOURCE  src/example.cpp -o debug/ruby_example-dir/src/example.cpp.o

#-- ruby_example: build source files
ruby_example-build: debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.o debug/ruby_example-dir/src/app.cpp.o debug/ruby_example-dir/src/example.cpp.o 

debug/libruby_example.so: debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.o debug/ruby_example-dir/src/app.cpp.o debug/ruby_example-dir/src/example.cpp.o 
	c++ -MMD --shared -fPIC debug/ruby_example-dir/debug/ruby_example_wrap-dir/src/example.i.cpp.o debug/ruby_example-dir/src/app.cpp.o debug/ruby_example-dir/src/example.cpp.o     -o debug/libruby_example.so

#-- ruby_example: link
ruby_example-shared: debug/libruby_example.so

#-- ruby_example: clean files in this target
ruby_example-clean:
	rm -f debug/ruby_example-dir/ruby_example_wrap-dir/src/*.o
	rm -f debug/ruby_example-dir/ruby_example_wrap-dir/src/*.d
	rm -f debug/ruby_example-dir/src/*.o
	rm -f debug/ruby_example-dir/src/*.d
	rm -f debug/libruby_example.so

# ==== python_example_wrap

#-- python_example_wrap: initialize for debug build
python_example_wrap-init:
	@mkdir -p debug
	@mkdir -p debug/python_example_wrap-dir/src

debug/python_example_wrap-dir/src/example.i.cpp: src/example.i
	swig -python -c++  -o debug/python_example_wrap-dir/src/example.i.cpp src/example.i 

#-- python_example_wrap: genned source files
python_example_wrap-gen: debug/python_example_wrap-dir/src/example.i.cpp 

#-- python_example_wrap: clean files in this target
python_example_wrap-clean:
	rm -f debug/python_example_wrap-dir/src/example.i.cpp/*.cpp

# ==== python_example

#-- python_example: initialize for debug build
python_example-init:
	@mkdir -p debug
	@mkdir -p debug/python_example-dir/debug/python_example_wrap-dir/src
	@mkdir -p debug/python_example-dir/src

-include debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.d
debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.o: debug/python_example_wrap-dir/src/example.i.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/python3.12"  -std=c++20 -D_GNU_SOURCE  debug/python_example_wrap-dir/src/example.i.cpp -o debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.o
-include debug/python_example-dir/src/app.cpp.d
debug/python_example-dir/src/app.cpp.o: src/app.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/python3.12"  -std=c++20 -D_GNU_SOURCE  src/app.cpp -o debug/python_example-dir/src/app.cpp.o
-include debug/python_example-dir/src/example.cpp.d
debug/python_example-dir/src/example.cpp.o: src/example.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/python3.12"  -std=c++20 -D_GNU_SOURCE  src/example.cpp -o debug/python_example-dir/src/example.cpp.o

#-- python_example: build source files
python_example-build: debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.o debug/python_example-dir/src/app.cpp.o debug/python_example-dir/src/example.cpp.o 

debug/libpython_example.so: debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.o debug/python_example-dir/src/app.cpp.o debug/python_example-dir/src/example.cpp.o 
	c++ -MMD --shared -fPIC debug/python_example-dir/debug/python_example_wrap-dir/src/example.i.cpp.o debug/python_example-dir/src/app.cpp.o debug/python_example-dir/src/example.cpp.o     -o debug/libpython_example.so

#-- python_example: link
python_example-shared: debug/libpython_example.so

#-- python_example: clean files in this target
python_example-clean:
	rm -f debug/python_example-dir/python_example_wrap-dir/src/*.o
	rm -f debug/python_example-dir/python_example_wrap-dir/src/*.d
	rm -f debug/python_example-dir/src/*.o
	rm -f debug/python_example-dir/src/*.d
	rm -f debug/libpython_example.so

# ==== node_example

#-- node_example: initialize for debug build
node_example-init:
	@mkdir -p debug
	@mkdir -p debug/node_example-dir/javascript
	@mkdir -p debug/node_example-dir/src

-include debug/node_example-dir/javascript/index.cpp.d
debug/node_example-dir/javascript/index.cpp.o: javascript/index.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/node" "-I/usr/local/share/swig/4.3.0"  -std=c++20 -D_GNU_SOURCE  javascript/index.cpp -o debug/node_example-dir/javascript/index.cpp.o
-include debug/node_example-dir/src/app.cpp.d
debug/node_example-dir/src/app.cpp.o: src/app.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/node" "-I/usr/local/share/swig/4.3.0"  -std=c++20 -D_GNU_SOURCE  src/app.cpp -o debug/node_example-dir/src/app.cpp.o
-include debug/node_example-dir/src/example.cpp.d
debug/node_example-dir/src/example.cpp.o: src/example.cpp
	c++ -MMD -fPIC  -c "-Isrc" "-I/usr/include/node" "-I/usr/local/share/swig/4.3.0"  -std=c++20 -D_GNU_SOURCE  src/example.cpp -o debug/node_example-dir/src/example.cpp.o

#-- node_example: build source files
node_example-build: debug/node_example-dir/javascript/index.cpp.o debug/node_example-dir/src/app.cpp.o debug/node_example-dir/src/example.cpp.o 

debug/libnode_example.so: debug/node_example-dir/javascript/index.cpp.o debug/node_example-dir/src/app.cpp.o debug/node_example-dir/src/example.cpp.o 
	c++ -MMD --shared -fPIC debug/node_example-dir/javascript/index.cpp.o debug/node_example-dir/src/app.cpp.o debug/node_example-dir/src/example.cpp.o     -o debug/libnode_example.so

#-- node_example: link
node_example-shared: debug/libnode_example.so

#-- node_example: clean files in this target
node_example-clean:
	rm -f debug/node_example-dir/javascript/*.o
	rm -f debug/node_example-dir/javascript/*.d
	rm -f debug/node_example-dir/src/*.o
	rm -f debug/node_example-dir/src/*.d
	rm -f debug/libnode_example.so

#-- clean files
clean: ruby_example_wrap-clean ruby_example-clean python_example_wrap-clean python_example-clean node_example-clean 

help:
	@printf "Available targets:\n"
	@printf "  [32;01mall                                [0m build all\n"
	@printf "  [32;01mclean                              [0m clean files\n"
	@printf "  [32;01mhelp                               [0m this help info\n"
	@printf "  [32;01mnode_example                       [0m build node_example\n"
	@printf "    [32;01mnode_example-build                 [0m node_example: build source files\n"
	@printf "    [32;01mnode_example-clean                 [0m node_example: clean files in this target\n"
	@printf "    [32;01mnode_example-init                  [0m node_example: initialize for debug build\n"
	@printf "    [32;01mnode_example-shared                [0m node_example: link\n"
	@printf "  [32;01mpython_example                     [0m build python_example\n"
	@printf "    [32;01mpython_example-build               [0m python_example: build source files\n"
	@printf "    [32;01mpython_example-clean               [0m python_example: clean files in this target\n"
	@printf "    [32;01mpython_example-init                [0m python_example: initialize for debug build\n"
	@printf "    [32;01mpython_example-shared              [0m python_example: link\n"
	@printf "  [32;01mpython_example_wrap                [0m build python_example_wrap\n"
	@printf "    [32;01mpython_example_wrap-clean          [0m python_example_wrap: clean files in this target\n"
	@printf "    [32;01mpython_example_wrap-gen            [0m python_example_wrap: genned source files\n"
	@printf "    [32;01mpython_example_wrap-init           [0m python_example_wrap: initialize for debug build\n"
	@printf "  [32;01mruby_example                       [0m build ruby_example\n"
	@printf "    [32;01mruby_example-build                 [0m ruby_example: build source files\n"
	@printf "    [32;01mruby_example-clean                 [0m ruby_example: clean files in this target\n"
	@printf "    [32;01mruby_example-init                  [0m ruby_example: initialize for debug build\n"
	@printf "    [32;01mruby_example-shared                [0m ruby_example: link\n"
	@printf "  [32;01mruby_example_wrap                  [0m build ruby_example_wrap\n"
	@printf "    [32;01mruby_example_wrap-clean            [0m ruby_example_wrap: clean files in this target\n"
	@printf "    [32;01mruby_example_wrap-gen              [0m ruby_example_wrap: genned source files\n"
	@printf "    [32;01mruby_example_wrap-init             [0m ruby_example_wrap: initialize for debug build\n"
	@printf "\n"
