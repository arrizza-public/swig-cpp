#ifndef APP_H
#define APP_H
// ============================================================================
// Copyright(c) All Rights Reserved
//
// Summary: App...
// ============================================================================
#include <string>

// --------------------
//! sample app for use in scripts
class App {
public:
    //! sample public variable
    double m_variable = 3.0;
    //! sample public function with parameters and return value
    //! @param s1   sample parameter
    //! @return string
    std::string append(const std::string& s1);
    //! sample public function with no parameters or return value
    void hello();
};

#endif // APP_H
