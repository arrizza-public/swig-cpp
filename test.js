const example = require("./out/js/example.node")

// TODO Uint8Array does not work;  sum_byte() "failed to get size of array
// var arr2 = new Uint8Array(4);
// arr2[0] = 1
// arr2[1] = 2
// arr2[2] = 3
// arr2[3] = 255
// console.log("sum_byte: " + example.sum_byte(arr2).toString())

// used for before/after columns
before = example.my_variable
example.my_variable = 5.1
after = example.my_variable

// used for sum_x() columns
arr = [0x01, 0x02, 0x03, 0xFF]

// used for append column
s1 = 'abc'

// used for class public variable
app = new example.App()
before2 = app.m_variable
app.m_variable = 5.2
after2 = app.m_variable

function print_value(i) {
    switch (i) {
        case '0':
            process.stdout.write("node".padStart(11))
            break;
        case '1':
            process.stdout.write(example.fact(6).toString().padStart(11))
            break;
        case '2':
            process.stdout.write(example.my_mod(11, 8).toString().padStart(11))
            break;
        case '3':
            process.stdout.write(before.toFixed(1).toString().padStart(11))
            break;
        case '4':
            process.stdout.write(after.toFixed(1).toString().padStart(11))
            break;
        case '5':
            process.stdout.write(example.sum_int(arr).toString().padStart(11))
            break;
        case '6':
            process.stdout.write(example.sum_byte(arr).toString().padStart(11))
            break;
        case '7':
            process.stdout.write(example.append(s1).padStart(11))
            break;
        case '8':
            process.stdout.write(before2.toFixed(1).toString().padStart(11))
            break;
        case '9':
            process.stdout.write(after2.toFixed(1).toString().padStart(11))
            break;
        case '10':
            process.stdout.write(app.append("xy").padStart(11))
            break;
        default:
            console.log("ERR unknown CLI arg: ", i)
    }
}

if (process.argv.length <= 2 || process.argv[2] === 'all') {
    for (var i = 0; i < 10; i++) {
        print_value(i.toString())
        console.log('')
    }
} else if (process.argv[2] === 'to_hex') {
    arr = [0x01, 0x02, 0x03, 0xFF, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10]
    console.log("node       :", example.to_hex(arr))
} else {
    print_value(process.argv[2])
    process.stdout.write(' ')
}

