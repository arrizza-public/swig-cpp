// ============================================================================
// Copyright(c) All Rights Reserved
//
// Summary: App...
// ============================================================================
#include "app.h"
#include <iostream>

std::string App::append(const std::string& s1)
{
    std::string result(s1);
    result.append("-app");
    return result;
}

void App::hello()
{
    std::cout << "hello" << std::endl;
}