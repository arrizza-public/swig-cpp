#ifndef NAPI_WRAP_H
#define NAPI_WRAP_H

#include <node_api.h>

// wrapper functions for the most simplest javascript scenarios

// --------------------
// declare argc and argv array based on the expected number of arguments to a function
// normally used with nwrap_parse_args
#define nwrap_declare_args(arg_count) \
    int argc = arg_count;             \
    napi_value argv[argc];            \
    napi_value jsthis

#define nwrap_declare_instance_method(name, func) \
    {                                             \
        name, 0, func, 0, 0, 0, napi_default, 0   \
    }

#define nwrap_declare_instance_var(name, getter, setter) \
    {                                                    \
        name, 0, 0, getter, setter, 0, napi_default, 0   \
    }

// --------------------
class NwrapPropertyDescriptorList {
public:
    // --------------------
    explicit NwrapPropertyDescriptorList(size_t max_size)
        : max_length(max_size)
        , length(0)
    {
        properties = (napi_property_descriptor*)malloc(max_size * sizeof(napi_property_descriptor));
    }

    // --------------------
    void add(napi_property_descriptor prop)
    {
        properties[length] = prop;
        length += 1;
        // TODO check length < max_length
    }

    const size_t max_length;
    size_t length;
    napi_property_descriptor* properties;
};

// --------------------
// parse the args from the javascript call
// TODO check if the number of args matches argc
void nwrap_parse_args(napi_env env, napi_callback_info info, size_t argc, napi_value* argv, napi_value* jsthis)
{
    // the jsthis variable is only used when there is a C++ object
    napi_status status = napi_get_cb_info(env, info, &argc, argv, jsthis, nullptr);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Failed to parse arguments");
    }
}

// --------------------
void nwrap_create_class(napi_env env, const char* name, NwrapPropertyDescriptorList& pdl, napi_callback n_new,
    napi_value& n_class)
{
    napi_status status = napi_define_class(env, name, NAPI_AUTO_LENGTH, n_new, nullptr, pdl.length, pdl.properties,
        &n_class);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to define App class");
    }
}

// --------------------
// get class instance
void nwrap_get_instance(napi_env env, napi_value jsthis, void** instance)
{
    napi_status status;
    status = napi_unwrap(env, jsthis, instance);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to unwrap instance");
    }
}

// --------------------
napi_value nwrap_get_instance_double(napi_env env, double val)
{
    napi_value n_result;

    napi_status status = napi_create_double(env, val, &n_result);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to get double value");
    }
    return n_result;
}

// --------------------
void nwrap_set_instance_double(napi_env env, napi_value new_value, double& field)
{
    napi_status status = napi_get_value_double(env, new_value, &field);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to get_value_double instance: App");
    }
}

// --------------------
// get an argument, expected type is integer
// TODO should this be int32_t?
int nwrap_get_int_arg(napi_env env, napi_value* argv, int index)
{
    int arg = 0;

    napi_status status = napi_get_value_int32(env, argv[index], &arg);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Invalid number was passed as argument");
    }
    return arg;
}

// --------------------
// copy js array of integers into vector
void nwrap_get_int_array(napi_env env, napi_value n_array, std::vector<int>& array)
{
    napi_status status;

    // get the number of elements in the array
    uint32_t array_size = 0;
    status = napi_get_array_length(env, n_array, &array_size);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "failed to get size of array");
    }

    // iterate over the array and copy into the vector
    for (int i = 0; i < array_size; ++i) {
        napi_value n_value;
        status = napi_get_element(env, n_array, i, &n_value);
        if (status != napi_ok) {
            napi_throw_error(env, nullptr, "failed to get i-th value from array");
        }

        int value = 0;
        status = napi_get_value_int32(env, n_value, &value);
        if (status != napi_ok) {
            napi_throw_error(env, nullptr, "expected i-th value in array to be an integer");
        }

        array.push_back(value);
    }
}

// --------------------
// copy js array of integers into vector
// TODO convert to a template
void nwrap_get_byte_array(napi_env env, napi_value n_array, std::vector<uint8_t>& array)
{
    napi_status status;

    // get the number of elements in the array
    uint32_t array_size = 0;
    status = napi_get_array_length(env, n_array, &array_size);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "failed to get size of array");
    }

    // iterate over the array and copy into the vector
    for (int i = 0; i < array_size; ++i) {
        napi_value n_value;
        status = napi_get_element(env, n_array, i, &n_value);
        if (status != napi_ok) {
            napi_throw_error(env, nullptr, "failed to get i-th value from array");
        }

        int value = 0;
        status = napi_get_value_int32(env, n_value, &value);
        if (status != napi_ok) {
            napi_throw_error(env, nullptr, "expected i-th value in array to be an integer");
        }

        array.push_back(value);
    }
}

// --------------------
// copy string into given argument
void nwrap_get_string_arg(napi_env env, napi_value n_string, std::string& arg0)
{
    size_t str_size = 0;
    napi_status status = napi_get_value_string_utf8(env, n_string, nullptr, 0, &str_size);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "cannot get size of utf8 string");
    }

    char buf[str_size];
    status = napi_get_value_string_utf8(env, n_string, buf, str_size + 1, &str_size);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "cannot get content from utf8 string");
    }

    arg0 = buf;
}

// --------------------
// generate a javascript value for the given arg
// TODO: should this be int32_t?
napi_value nwrap_gen_int_result(napi_env env, int arg)
{
    napi_value n_val;

    napi_status status = napi_create_int32(env, arg, &n_val);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to create int value");
    }

    return n_val;
}

// --------------------
// generate a javascript string result
napi_value nwrap_gen_string_result(napi_env env, const std::string& arg)
{
    napi_value n_val;

    napi_status status = napi_create_string_utf8(env, arg.c_str(), arg.size(), &n_val);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to create string value");
    }

    return n_val;
}

// --------------------
void nwrap_add_property_to_exports(napi_env env, napi_value exports, const char* name, napi_value property)
{
    napi_status status = napi_set_named_property(env, exports, name, property);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to add property to exports");
    }
}

// --------------------
// declare a javascript function, add it to exports
void nwrap_declare_func(napi_env env, napi_value exports, napi_callback cb, const char* func_name)
{
    napi_value n_fn;

    napi_status status = napi_create_function(env, nullptr, 0, cb, nullptr, &n_fn);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to wrap function");
    }

    nwrap_add_property_to_exports(env, exports, func_name, n_fn);
}

// --------------------
// declare a javascript variable, add it to exports
// TODO can there be a read-only variable?
void nwrap_declare_var(napi_env env, napi_value exports, napi_value var, const char* var_name)
{
    napi_status status = napi_create_double(env, my_variable, &var);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to wrap value: my_variable");
    }

    nwrap_add_property_to_exports(env, exports, var_name, var);
}

#endif // NAPI_WRAP_H
