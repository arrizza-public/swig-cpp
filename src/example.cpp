/* File : example.cpp */
#include "./example.h"
#include <cstring>

double my_variable = 3.0;

// --------------------
// Compute factorial of n
int fact(int n)
{
    if (n <= 1) {
        return 1;
    }
    return n * fact(n - 1);
}

// --------------------
// Compute n mod m
int my_mod(int n, int m)
{
    return n % m;
}

// --------------------
// return the sum of integers in the incoming buffer
int sum_int(const std::vector<int>& buf)
{
    int sum = 0;
    for (auto& x : buf) {
        sum += x;
    }

    return sum;
}

// --------------------
// return the sum of bytes in the incoming buffer
int sum_byte(const std::vector<uint8_t>& buf)
{
    int sum = 0;
    for (auto& x : buf) {
        sum += x;
    }

    return sum;
}

// --------------------
// add "-ok" to the incoming string
std::string append(const std::string& s1)
{
    std::string result(s1);
    result.append("-ok");
    return result;
}

// --------------------
// convert a buffer of the given length into a hex string
// with 16 values per line
std::string to_hex(const std::vector<uint8_t>& buf)
{
    std::string hex;

    constexpr int INITIAL_SPACES = 3;
    constexpr int PER_BYTE = 3;
    constexpr int GAP = 2;

    char buffer[256];

    int j = INITIAL_SPACES;
    ::strncpy(buffer, "   ", INITIAL_SPACES);

    for (size_t i = 0; i < buf.size(); ++i) {
        if (i != 0 && (i % 16) == 0) {
            // new line after 16 digits
            hex += buffer;
            hex += "\n";
            j = INITIAL_SPACES;
            ::strncpy(buffer, "   ", INITIAL_SPACES);
        }

        if (j == (8 * PER_BYTE + INITIAL_SPACES)) {
            // add a small gap after the 8th digit
            ::strcat(buffer, "  ");
            j += GAP;
        }

        ::sprintf(&buffer[j], "%02X ", buf[i]);
        j += PER_BYTE;
    }

    hex += buffer;
    hex += "\n";

    return hex;
}