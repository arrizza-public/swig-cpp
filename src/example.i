// the format of .i files is not recognized by clang-format
// clang-format off

/* File : example.i */
%module example

%{
#include "../src/example.h"
#include "../src/app.h"
%}

%include <std_string.i>
%include <stl.i>
%include <std_vector.i>
%include <stdint.i>
%include <cpointer.i>
%include <carrays.i>

namespace std{
        %template(IntVector)    vector<int>;
        %template(ByteVector)   vector<uint8_t>;
}

%include "../src/example.h"
%include "../src/app.h"
