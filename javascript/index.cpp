// Note: this file will not be needed when SWIG works with js v12
// see https://hackernoon.com/n-api-and-getting-started-with-writing-c-addons-for-node-js-cf061b3eae75
// for a great tutorial on N-API
// see https://nodejs.org/api/n-api.html for N-API doc

#include "../src/app.h"
#include "../src/example.h"
#include "nwrap.h"
#include <iostream>

// my_variable global variable
static napi_value n_my_variable;

// --------------------
// handle fact()
napi_value n_fact(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    int arg1 = nwrap_get_int_arg(env, argv, 0);
    int result = fact(arg1);
    napi_value n_result = nwrap_gen_int_result(env, result);

    return n_result;
}

// --------------------
// handle my_mod()
napi_value n_my_mod(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(2);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    int arg1 = nwrap_get_int_arg(env, argv, 0);
    int arg2 = nwrap_get_int_arg(env, argv, 1);
    int result = my_mod(arg1, arg2);
    napi_value n_result = nwrap_gen_int_result(env, result);

    return n_result;
}

// --------------------
// handle sum_int()
napi_value n_sum_int(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    std::vector<int> arg1;
    nwrap_get_int_array(env, argv[0], arg1);
    int result = sum_int(arg1);
    napi_value n_result = nwrap_gen_int_result(env, result);

    return n_result;
}

// --------------------
// handle sum_byte()
napi_value n_sum_byte(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    std::vector<uint8_t> arg1;
    nwrap_get_byte_array(env, argv[0], arg1);
    int result = sum_byte(arg1);
    napi_value n_result = nwrap_gen_int_result(env, result);

    return n_result;
}

// --------------------
// handle append()
napi_value n_append(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    std::string arg0;
    nwrap_get_string_arg(env, argv[0], arg0);
    std::string result = append(arg0);
    napi_value n_result = nwrap_gen_string_result(env, result);

    return n_result;
}

// --------------------
// handle to_hex()
napi_value n_to_hex(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);
    std::vector<uint8_t> arg1;
    nwrap_get_byte_array(env, argv[0], arg1);
    std::string result = to_hex(arg1);
    napi_value n_result = nwrap_gen_string_result(env, result);

    return n_result;
}

// --------------------
void n_app_dtor(napi_env env, void* instance, void* /* hint */)
{
    delete ((App*)instance);
}

// --------------------
// handle new App()
napi_value n_app_new(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);

    napi_status status;
    napi_value n_result;
    status = napi_create_object(env, &n_result);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to create object");
    }

    App* instance = new App();

    status = napi_wrap(env, jsthis, (void*)instance, n_app_dtor, nullptr, nullptr);
    if (status != napi_ok) {
        napi_throw_error(env, nullptr, "Unable to create App instance");
    }

    // no parameters to new()

    return jsthis;
}

// --------------------
// handle get of App::m_variable
napi_value n_app_get_m_variable(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);

    App* instance = nullptr;
    nwrap_get_instance(env, jsthis, (void**)&instance);
    napi_value n_result = nwrap_get_instance_double(env, instance->m_variable);
    return n_result;
}

// --------------------
// handle get of App::m_variable
napi_value n_app_set_m_variable(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);

    App* instance = nullptr;
    nwrap_get_instance(env, jsthis, (void**)&instance);
    nwrap_set_instance_double(env, argv[0], instance->m_variable);

    return nullptr;
}

// --------------------
// handle get of App::append()
napi_value n_app_append(napi_env env, napi_callback_info info)
{
    nwrap_declare_args(1);
    nwrap_parse_args(env, info, argc, argv, &jsthis);

    App* instance = nullptr;
    nwrap_get_instance(env, jsthis, (void**)&instance);

    std::string arg0;
    nwrap_get_string_arg(env, argv[0], arg0);
    std::string result = instance->append(arg0);
    napi_value n_result = nwrap_gen_string_result(env, result);

    return n_result;
}

// --------------------
// generate export for App class
void gen_app(napi_env env, napi_value exports)
{
    NwrapPropertyDescriptorList pdl(5);
    pdl.add(nwrap_declare_instance_var("m_variable", n_app_get_m_variable, n_app_set_m_variable));
    pdl.add(nwrap_declare_instance_method("append", n_app_append));

    napi_value n_class;
    nwrap_create_class(env, "App", pdl, n_app_new, n_class);

    nwrap_add_property_to_exports(env, exports, "App", n_class);
}

// --------------------
// generate javascript exports
napi_value init(napi_env env, napi_value exports)
{
    nwrap_declare_func(env, exports, n_fact, "fact");
    nwrap_declare_func(env, exports, n_my_mod, "my_mod");
    nwrap_declare_var(env, exports, n_my_variable, "my_variable");
    nwrap_declare_func(env, exports, n_sum_int, "sum_int");
    nwrap_declare_func(env, exports, n_sum_byte, "sum_byte");
    nwrap_declare_func(env, exports, n_append, "append");
    nwrap_declare_func(env, exports, n_to_hex, "to_hex");
    gen_app(env, exports);

    return exports;
}

NAPI_MODULE(example, init);
