from pyalamake import alamake


# --------------------
def gen_ruby():
    tgt = alamake.create('ruby_example_wrap', 'swig')
    tgt.engine('ruby')
    tgt.set_cpp()  # gens .cpp
    tgt.add_sources([
        'src/example.i',
    ])

    tgt = alamake.create('ruby_example', 'cpp-lib')
    tgt.set_type('shared')
    tgt.add_sources([
        'debug/ruby_example_wrap-dir/src/example.i.cpp',
        'src/app.cpp',
        'src/app.h',
        'src/example.cpp',
        'src/example.h',
    ])
    tgt.add_include_directories([
        'src',
    ])
    tgt.add_include_directories(alamake.osal.ruby_includes())
    tgt.add_link_directories(alamake.osal.ruby_link_dirs())
    # TODO set output name to "??"


# --------------------
def gen_python():
    tgt = alamake.create('python_example_wrap', 'swig')
    tgt.engine('python')
    tgt.set_cpp()  # gens .cpp
    tgt.add_sources([
        'src/example.i',
    ])

    tgt = alamake.create('python_example', 'cpp-lib')
    tgt.set_type('shared')
    tgt.add_sources([
        'debug/python_example_wrap-dir/src/example.i.cpp',
        'src/app.cpp',
        'src/app.h',
        'src/example.cpp',
        'src/example.h',
    ])
    tgt.add_include_directories([
        'src',
    ])
    tgt.add_include_directories(alamake.osal.python_includes())
    # TODO set output name to "??"


# --------------------
def gen_node():
    # TODO not used
    # tgt = alamake.create('node_example_wrap', 'swig')
    # tgt.engine('v8')
    # tgt.set_cpp()  # gens .cpp
    # tgt.add_sources([
    #     'src/example.i',
    # ])

    tgt = alamake.create('node_example', 'cpp-lib')
    tgt.set_type('shared')
    tgt.add_sources([
        # 'debug/node_example_wrap-dir/src/example.i.cpp',
        'javascript/index.cpp',  # TODO temp until swig v8 or napi is fixed
        'src/app.cpp',
        'src/app.h',
        'src/example.cpp',
        'src/example.h',
    ])
    tgt.add_include_directories([
        'src',
        '/usr/include/node',
        '/usr/local/share/swig/4.3.0',
    ])
    # TODO set output name to "example.node"


# --------------------
# === mainline
gen_ruby()
gen_python()
gen_node()

# === generate makefile for all targets
alamake.makefile()
